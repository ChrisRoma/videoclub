
package peliculas.negocio;

public interface CatalogoPeliculas {
    void agregarPelicula(String nombrePelicula , String nombreArchivo);
    
    void listarPelicula(String archivo);
    
    void buscarPelicula(String nombreArchivo , String buscar);
    
    void iniciarArchivo(String nombreArchivo);
}
