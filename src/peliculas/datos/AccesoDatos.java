
package peliculas.datos;

import java.util.List;
import peliculas.domain.*;
public interface AccesoDatos {
   
    boolean existe(String nombreArchivo);
    List<Pelicula> listar(String Nombre);
    void escribir(Pelicula pelicula , String nombreArchivo, boolean anexar);
    String buscar(String nombreArchivo,String buscar);
    void crear(String nombreArchivo);
    void borrar(String nombreArchivo);
}
